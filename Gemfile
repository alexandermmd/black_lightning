source 'http://rubygems.org'

gem 'rails', '~> 6.0'

gem 'mysql2'

gem 'sass-rails',   '> 5.0.0'
gem 'uglifier', '>= 1.3.0'
gem 'webpacker', '~> 5.x'

gem 'coffee-rails'
gem 'bootstrap-sass', '< 3.0'
gem 'font-awesome-sass'
gem 'fancybox3'

gem 'simple_form'
gem 'devise'
gem 'doorkeeper'
gem 'doorkeeper-openid_connect'
gem 'cancancan'
gem 'rolify'
gem 'recaptcha'

gem 'will_paginate'
gem 'kramdown'
gem 'nested_form'
gem 'twitter'
gem 'json', '1.8.5'
gem 'rails_autolink'

gem 'delayed_job_active_record'
gem 'daemons'

gem 'caxlsx'
gem 'prawn'
gem 'rqrcode'

gem 'silencer'

gem 'image_processing'
gem 'mini_magick'
gem 'active_storage_validations'
gem 'aws-sdk-s3', require: false

gem 'chronic'
gem 'ransack'

gem 'nokogiri'

gem 'rack-cors'
gem 'paper_trail'

gem 'stringex'

gem 'honeybadger', '~> 4.0'

group :development, :test do
  gem 'byebug'
  gem 'spring'

  gem 'better_errors'
  gem 'binding_of_caller'

  gem 'rdoc'
  gem 'rubocop'
  gem 'rubocop-faker'
  gem 'rails-controller-testing'

  gem 'factory_bot_rails'

  gem 'tzinfo-data'
  gem 'coffee-script-source', '1.8.0'

  gem 'annotate'

  gem 'bullet'

  gem 'faker'
end

group :test do
  gem 'simplecov'
  gem 'simplecov-rcov'

  gem 'html_acceptance'
end

# Deploy with Capistrano
gem 'capistrano-rails'
gem 'capistrano-rvm'
gem 'capistrano3-delayed-job'
